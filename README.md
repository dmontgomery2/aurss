# aurss

RSS reader application.

- Run lint: `nps test.lint`
- Run the test suite: `nps test`
- Run the test suite in watch mode: `nps test.jest.watch`
- Build the project for production: `nps build`
- Start the project: `nps`
- Start the project with hot module reloading: `au run --watch`
- Extract transaltions: `nps translations`

## Adding an RSS backend

1. Implement the backend. It must implement the `Backend` interface.
2. Make it selectable by adding it to the `SelectableBackend` enum.

## Creating a new version

1. Bump version in `constants.ts`
2. Create a git tag.
