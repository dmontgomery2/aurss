import { autoinject } from "aurelia-framework";
import { Store, dispatchify } from "aurelia-store";
import { Subscription } from "rxjs";
import { pluck } from "rxjs/operators";
import * as constants from "../constants";
import { Article } from "../models/Article";
import * as rssActions from "../store/rss-backends-actions";
import { State } from "../store/state";
import "./display-articles-from-category.scss";

@autoinject()
export class DisplayArticlesFromCategory {
    public articles: Article[] = [];
    public isFetching = false;
    public title = "";
    public allowMarkAsReadOnScroll: boolean;
    public markArticleAsRead = dispatchify(rssActions.markAsRead);
    public markArticleAsReadAndDiscard: (article: Article) => void;
    public markArticleAsUnread = dispatchify(rssActions.markAsUnread);
    public markArticleAsFavorite = dispatchify(rssActions.markAsFavorite);
    public unmarkArticleAsFavorite = dispatchify(rssActions.unmarkAsFavorite);
    public openArticle: (article: Article) => void;
    private subscriptions: Subscription[] = [];

    public constructor(private store: Store<State>) {
        this.openArticle = (article) =>
            this.store
                .pipe(rssActions.openArticle, article)
                .pipe(rssActions.markAsRead, article)
                .dispatch();
        this.markArticleAsReadAndDiscard = (article) => {
            return this.markArticleAsRead(article, { discard: true });
        };
    }

    public activate({ categoryId = constants.DEFAULT_CATEGORY } = {}): void {
        this.store.dispatch(rssActions.fetchArticles, categoryId);
        this.subscriptions.push(
            this.store.state.pipe(pluck("rss")).subscribe((rssData) => {
                this.articles = rssData.displayedArticles;
                this.isFetching = rssData.isFetching;
                const categories = rssData.categories.filter(
                    (category) => category.id.toString() === categoryId,
                );
                if (categories.length > 0) {
                    this.title = categories[0].title;
                }
            }),
        );
        this.subscriptions.push(
            this.store.state
                .pipe(pluck("options", "markAsReadOnScroll"))
                .subscribe(
                    (markAsReadOnScroll) => (this.allowMarkAsReadOnScroll = markAsReadOnScroll),
                ),
        );
    }

    public deactivate(): void {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }
}
