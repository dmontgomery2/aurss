import { autoinject, bindable } from "aurelia-framework";
import { Store } from "aurelia-store";
import { Subscription as RxjsSubscription } from "rxjs/internal/Subscription";
import { Options, State } from "../../store/state";

@autoinject()
export class AurssMarkAsReadOnScrollCustomAttribute {
    @bindable() private markAsRead;
    @bindable() private article;
    private triggered: boolean;
    private readonly scrollEventListener;
    private options?: Options;
    private storeSubscriptions: RxjsSubscription[] = [];

    public constructor(private element: Element, private store: Store<State>) {
        this.triggered = false;
        this.scrollEventListener = () => {
            // If the article is already read there is nothing to do.
            if (this.article.isRead) {
                return;
            } else if (this.triggered) {
                // If this was already triggered, there is nothing to do.
                return;
            } else if (!this.options || !this.options.markAsReadOnScroll) {
                return;
            }

            if (this.element.getBoundingClientRect().top < 0) {
                this.triggered = true;
                this.markAsRead(this.article);
            }
        };
    }

    public attached(): void {
        // We only do this on desktop. Swipe can be used on touch devices.
        if ("ontouchstart" in window) {
            return;
        }

        this.storeSubscriptions.push(
            this.store.state.subscribe((state) => (this.options = state.options)),
        );
        window.addEventListener("scroll", this.scrollEventListener);
    }

    public detached(): void {
        // We only do this on desktop. Swipe can be used on touch devices.
        if ("ontouchstart" in window) {
            return;
        }

        this.storeSubscriptions.forEach((sub) => sub.unsubscribe());
        window.removeEventListener("scroll", this.scrollEventListener);
    }
}
