module.exports = {
    singleQuote: false,
    trailingComma: "all",
    jsxBracketSameLine: false,
    printWidth: 100,
    bracketSpacing: true,
};
