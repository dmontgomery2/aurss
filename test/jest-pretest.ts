import { Container } from "aurelia-framework";
import { Options } from "aurelia-loader-nodejs";
import { globalize } from "aurelia-pal-nodejs";
import "aurelia-polyfills";
import { GlobalWithFetchMock } from "jest-fetch-mock";
import * as path from "path";
import { setAutoFreeze } from "immer";

setAutoFreeze(false);

/* eslint-disable @typescript-eslint/no-explicit-any */

Options.relativeToDir = path.join(__dirname, "unit");
globalize();

(global as any).navigator = {};

const customGlobal: GlobalWithFetchMock = global as unknown as GlobalWithFetchMock;
customGlobal.fetch = require("jest-fetch-mock");
customGlobal.fetchMock = customGlobal.fetch;

const container = new Container();
container.makeGlobal();
