import { Store } from "aurelia-store";
import { State } from "../../src/store/state";
import { createInitialState } from "./state-fixtures";

export const createStore = (state = {}): Store<State> => {
    const initialState = {
        ...createInitialState(),
        ...state,
    };
    return new Store(initialState);
};
